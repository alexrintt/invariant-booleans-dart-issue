void main() {}

class MyClass {
  final List<String> _mylist = ['a', 'b', 'c', 'd', 'a'];

  var _i = 0;

  int _next() => _i++;

  String _current() => _mylist[_i];

  void unexpectedBehaviorSample() {
    if (_current() == 'a') {
      /// ...

      _next(); // Changes the value of `current()` to `b`

      if (_current() != 'a') {
        /// ...
      }

      /// ...
    }
  }
}
